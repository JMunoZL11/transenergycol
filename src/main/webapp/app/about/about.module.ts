import { ABOUT_ROUTE } from './about.route';
import { RouterModule } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AboutComponent } from './about.component';
import { TransenergycolSharedModule } from '../shared';

@NgModule({
  imports: [
      TransenergycolSharedModule,
      RouterModule.forChild([ ABOUT_ROUTE ])
  ],
  declarations: [
      AboutComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransenergycolAboutModule {}
