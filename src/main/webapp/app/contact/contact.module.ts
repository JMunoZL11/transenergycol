import { RouterModule } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TransenergycolSharedModule } from '../shared';
import { CONTACT_ROUTE } from './contact.route';
import { ContactComponent } from './contact.component';

@NgModule({
  imports: [
      TransenergycolSharedModule,
      RouterModule.forChild([ CONTACT_ROUTE ])
  ],
  declarations: [
      ContactComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransenergycolContactModule {}
