/**
 * View Models used by Spring MVC REST controllers.
 */
package com.transenergycol.web.rest.vm;
