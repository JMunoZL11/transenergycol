import { AboutComponent } from './about.component';
import { Route } from '@angular/router';

export const ABOUT_ROUTE: Route = {
    path: 'nosotros',
    component: AboutComponent,
    data: {
        authorities: [],
        pageTitle: 'Nosotros - Transenergycol'
    }
};
