import { PortfolioComponent } from './portfolio.component';
import { Route } from '@angular/router';

export const PORTFOLIO_ROUTE: Route = {
    path: 'productos-y-servicios',
    component: PortfolioComponent,
    data: {
        authorities: [],
        pageTitle: 'Productos y Servicios - Transenergycol'
    }
};
