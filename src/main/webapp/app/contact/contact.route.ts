import { ContactComponent } from './contact.component';
import { Route } from '@angular/router';

export const CONTACT_ROUTE: Route = {
    path: 'contacto',
    component: ContactComponent,
    data: {
        authorities: [],
        pageTitle: 'Contactanos - Transenergycol'
    }
};
