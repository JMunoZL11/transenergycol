import { PortfolioComponent } from './portfolio.component';
import { RouterModule } from '@angular/router';
import { PORTFOLIO_ROUTE } from './portfolio.route';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransenergycolSharedModule } from '../shared';

@NgModule({
  imports: [
    TransenergycolSharedModule,
    RouterModule.forChild([ PORTFOLIO_ROUTE ])
  ],
  declarations: [
      PortfolioComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransenergycolPortfolioModule { }
